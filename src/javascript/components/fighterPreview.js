import { createElement } from '../helpers/domHelper';


export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const { name, health, attack, defense } = fighter;

  const nameElement = createElement({ tagName: 'span', className: 'fighter-preview___name' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-preview___health' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-preview___attack' });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-preview___defense' });
  const imageElement = createFighterImage(fighter);

  nameElement.innerText = name;
  healthElement.innerText = health;
  attackElement.innerText = attack;
  defenseElement.innerText = defense;

  fighterElement.append(nameElement, healthElement, attackElement, defenseElement, imageElement);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
