"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.getBlockPower = exports.getHitPower = exports.getDamage = exports.fight = void 0;
var controls_1 = require("../../constants/controls");
function fight(firstFighter, secondFighter) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, new Promise(function (resolve) {
                    var CRITICAL_HIT_COOL_DOWN = 10000;
                    var playerOneFullHealth = firstFighter.health;
                    var playerTwoFullHealth = secondFighter.health;
                    var pressed = new Set();
                    var playerOneHealth = firstFighter.health;
                    var playerTwoHealth = secondFighter.health;
                    var winner = {};
                    var isPlayerOneAttackBtnPressed = true;
                    var isPlayerTwoAttackBtnPressed = true;
                    var isPlayerOneCriticalHitPressed = true;
                    var isPlayerTwoCriticalHitPressed = true;
                    var leftHealthIndicator = document.querySelector('#left-fighter-indicator');
                    var rightHealthIndicator = document.querySelector('#right-fighter-indicator');
                    document.addEventListener('keydown', function keyDownHandler(event) {
                        pressed.add(event.code);
                        var isPlayerOneAttackAndNotBlock = isPlayerAttack(pressed, controls_1.controls.PlayerOneAttack, isPlayerOneAttackBtnPressed) && !isPlayerBlock(pressed, controls_1.controls.PlayerOneBlock);
                        var isPlayerTwoAttackAndNotBlock = isPlayerAttack(pressed, controls_1.controls.PlayerTwoAttack, isPlayerTwoAttackBtnPressed) && !isPlayerBlock(pressed, controls_1.controls.PlayerTwoBlock);
                        var playerOneCriticalHitAndNotBlock = isPlayerHasCriticalHit(pressed, controls_1.controls.PlayerOneCriticalHitCombination) && !isPlayerBlock(pressed, controls_1.controls.PlayerOneBlock);
                        var playerTwoCriticalHitAndNotBlock = isPlayerHasCriticalHit(pressed, controls_1.controls.PlayerTwoCriticalHitCombination) && !isPlayerBlock(pressed, controls_1.controls.PlayerTwoBlock);
                        if (isPlayerOneAttackAndNotBlock) {
                            if (isPlayerBlock(pressed, controls_1.controls.PlayerTwoBlock)) {
                                return playerTwoHealth -= 0;
                            }
                            playerTwoHealth -= getDamage(firstFighter, secondFighter);
                            changeHealthIndicatorWidth(rightHealthIndicator, playerTwoFullHealth, playerTwoHealth);
                            isPlayerOneAttackBtnPressed = false;
                        }
                        if (isPlayerTwoAttackAndNotBlock) {
                            if (isPlayerBlock(pressed, controls_1.controls.PlayerOneBlock)) {
                                return playerOneHealth -= 0;
                            }
                            playerOneHealth -= getDamage(secondFighter, firstFighter);
                            changeHealthIndicatorWidth(leftHealthIndicator, playerOneFullHealth, playerOneHealth);
                            isPlayerTwoAttackBtnPressed = false;
                        }
                        if (playerOneCriticalHitAndNotBlock) {
                            if (isPlayerOneCriticalHitPressed) {
                                playerTwoHealth -= getCriticalHitPower(firstFighter);
                                changeHealthIndicatorWidth(rightHealthIndicator, playerTwoFullHealth, playerTwoHealth);
                                isPlayerOneCriticalHitPressed = false;
                                setTimeout(function () { isPlayerOneCriticalHitPressed = true; }, CRITICAL_HIT_COOL_DOWN);
                            }
                        }
                        if (playerTwoCriticalHitAndNotBlock) {
                            if (isPlayerTwoCriticalHitPressed) {
                                playerOneHealth -= getCriticalHitPower(secondFighter);
                                changeHealthIndicatorWidth(leftHealthIndicator, playerOneFullHealth, playerOneHealth);
                                isPlayerTwoCriticalHitPressed = false;
                                setTimeout(function () { isPlayerTwoCriticalHitPressed = true; }, CRITICAL_HIT_COOL_DOWN);
                            }
                        }
                        if (playerOneHealth <= 0) {
                            winner = secondFighter;
                            resolve(winner);
                        }
                        if (playerTwoHealth <= 0) {
                            winner = firstFighter;
                            resolve(winner);
                        }
                    });
                    document.addEventListener('keyup', function keyUpHandler(event) {
                        pressed["delete"](event.code);
                        isPlayerOneAttackBtnPressed = true;
                        isPlayerTwoAttackBtnPressed = true;
                    });
                })];
        });
    });
}
exports.fight = fight;
function getDamage(attacker, defender) {
    var damage = getHitPower(attacker) - getBlockPower(defender);
    return damage > 0 ? damage : 0;
}
exports.getDamage = getDamage;
function getHitPower(fighter) {
    return fighter.attack * getRandomNum();
}
exports.getHitPower = getHitPower;
function getBlockPower(fighter) {
    return fighter.defense * getRandomNum();
}
exports.getBlockPower = getBlockPower;
function getRandomNum() {
    return Math.random() * (2 - 1) + 1;
}
function isPlayerAttack(pressed, playerAttackBtn, isPlayerAttackBtnPressed) {
    return pressed.has(playerAttackBtn) && isPlayerAttackBtnPressed;
}
function isPlayerBlock(pressed, playerBlock) {
    return pressed.has(playerBlock);
}
function isPlayerHasCriticalHit(pressed, playerCriticalHitCombination) {
    return pressed.has(playerCriticalHitCombination[0])
        && pressed.has(playerCriticalHitCombination[1])
        && pressed.has(playerCriticalHitCombination[2]);
}
function getCriticalHitPower(fighter) {
    return 2 * fighter.attack;
}
function changeHealthIndicatorWidth(indicator, playerFullHealth, playerHealth) {
    var playerDiffHealthInPercent = (playerFullHealth - playerHealth) / playerFullHealth * 100;
    indicator.style.width = 100 - playerDiffHealthInPercent + "%";
}
