import { controls } from '../../constants/controls';

export async function fight(firstFighter: any, secondFighter: any): Promise<object> {
  return new Promise((resolve) => {

    const CRITICAL_HIT_COOL_DOWN: number = 10000;
    
    const playerOneFullHealth: number = firstFighter.health;
    const playerTwoFullHealth: number = secondFighter.health;

    const pressed: Set<string> = new Set();

    let playerOneHealth: number = firstFighter.health;
    let playerTwoHealth: number = secondFighter.health;
    let winner: object = {};

    let isPlayerOneAttackBtnPressed: boolean = true;
    let isPlayerTwoAttackBtnPressed: boolean = true;

    let isPlayerOneCriticalHitPressed: boolean = true;
    let isPlayerTwoCriticalHitPressed: boolean = true;

    let leftHealthIndicator: HTMLElement | null = document.querySelector('#left-fighter-indicator');
    let rightHealthIndicator: HTMLElement | null = document.querySelector('#right-fighter-indicator');

    document.addEventListener('keydown', function keyDownHandler(event) {

      pressed.add(event.code);

      const isPlayerOneAttackAndNotBlock: boolean = isPlayerAttack(pressed, controls.PlayerOneAttack, isPlayerOneAttackBtnPressed) && !isPlayerBlock(pressed, controls.PlayerOneBlock);
      const isPlayerTwoAttackAndNotBlock: boolean = isPlayerAttack(pressed, controls.PlayerTwoAttack, isPlayerTwoAttackBtnPressed) && !isPlayerBlock(pressed, controls.PlayerTwoBlock); 
      const playerOneCriticalHitAndNotBlock: boolean = isPlayerHasCriticalHit(pressed, controls.PlayerOneCriticalHitCombination) && !isPlayerBlock(pressed, controls.PlayerOneBlock);
      const playerTwoCriticalHitAndNotBlock: boolean = isPlayerHasCriticalHit(pressed, controls.PlayerTwoCriticalHitCombination) && !isPlayerBlock(pressed, controls.PlayerTwoBlock);
      
      if (isPlayerOneAttackAndNotBlock) {
        if (isPlayerBlock(pressed, controls.PlayerTwoBlock)) {
          return playerTwoHealth -= 0;
        }
    
        playerTwoHealth -= getDamage(firstFighter, secondFighter);
        changeHealthIndicatorWidth(rightHealthIndicator, playerTwoFullHealth, playerTwoHealth);
        isPlayerOneAttackBtnPressed = false;
      }

      if (isPlayerTwoAttackAndNotBlock) {
        if (isPlayerBlock(pressed, controls.PlayerOneBlock)) {
          return playerOneHealth -= 0;
        }

        playerOneHealth -= getDamage(secondFighter, firstFighter);
        changeHealthIndicatorWidth(leftHealthIndicator, playerOneFullHealth, playerOneHealth);
        isPlayerTwoAttackBtnPressed = false;
      }

      if (playerOneCriticalHitAndNotBlock) {
        if (isPlayerOneCriticalHitPressed) {
          playerTwoHealth -= getCriticalHitPower(firstFighter);
          changeHealthIndicatorWidth(rightHealthIndicator, playerTwoFullHealth, playerTwoHealth);
          isPlayerOneCriticalHitPressed = false;
          setTimeout(() => { isPlayerOneCriticalHitPressed = true; }, CRITICAL_HIT_COOL_DOWN);
        }
      }

      if (playerTwoCriticalHitAndNotBlock) {
        if (isPlayerTwoCriticalHitPressed) {
          playerOneHealth -= getCriticalHitPower(secondFighter);
          changeHealthIndicatorWidth(leftHealthIndicator, playerOneFullHealth, playerOneHealth);
          isPlayerTwoCriticalHitPressed = false;
          setTimeout(() => { isPlayerTwoCriticalHitPressed = true; }, CRITICAL_HIT_COOL_DOWN);
        }
      }

      if (playerOneHealth <= 0) {
        winner = secondFighter;
        resolve(winner);
      }

      if (playerTwoHealth <= 0) {
        winner = firstFighter;
        resolve(winner);
      }

    });

    document.addEventListener('keyup', function keyUpHandler(event) {
      pressed.delete(event.code);
      isPlayerOneAttackBtnPressed = true;
      isPlayerTwoAttackBtnPressed = true;
    });

  });
}

export function getDamage(attacker: object, defender: object): number {
  let damage = getHitPower(attacker) - getBlockPower(defender);

  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter: any): number {
  return fighter.attack * getRandomNum();
}

export function getBlockPower(fighter: any): number {
  return fighter.defense * getRandomNum();
}

function getRandomNum(): number {
  return Math.random() * (2 - 1) + 1;
}

function isPlayerAttack(pressed: Set<string>, playerAttackBtn: string, isPlayerAttackBtnPressed: boolean): boolean {
  return pressed.has(playerAttackBtn) && isPlayerAttackBtnPressed;
}

function isPlayerBlock(pressed: Set<string>, playerBlock: string): boolean {
  return pressed.has(playerBlock);
}

function isPlayerHasCriticalHit(pressed: Set<string>, playerCriticalHitCombination: Array<string>): boolean {
  return pressed.has(playerCriticalHitCombination[0])
    && pressed.has(playerCriticalHitCombination[1])
    && pressed.has(playerCriticalHitCombination[2]);
}

function getCriticalHitPower(fighter: any): number {
  return 2 * fighter.attack;
}

function changeHealthIndicatorWidth(indicator: HTMLElement | null, playerFullHealth: number, playerHealth: number): void {
  let playerDiffHealthInPercent: number = (playerFullHealth - playerHealth) / playerFullHealth * 100;
  indicator.style.width = `${100 - playerDiffHealthInPercent}%`;
}

