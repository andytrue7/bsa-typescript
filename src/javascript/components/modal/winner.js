import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  const fighterImage = createFighterImage(fighter);
  showModal({ title: 'The winner is:', bodyElement: fighterImage});
}
